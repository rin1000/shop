class CartitemController < ApplicationController
  def create
    logger.debug "ID:"
    logger.debug params[:id]
    cartitem = Cartitem.find_by(product_id: params[:id],cart_id: session[:cart_id])
    if cartitem
      cartitem.qty += 1
    else
      cartitem = Cartitem.new(qty:1,product_id: params[:id], cart_id: session[:cart_id])
    end
    cartitem.save
    redirect_to cart_show_path
  end

  def destroy
    Cartitem.find(params[:id]).destroy
    redirect_to cart_show_path
  end
end
