Rails.application.routes.draw do
  resources :product,only: [:new,:create,:destroy]
  resources :cartitem,only: [:create,:destroy]
  
  get 'cart/show'
  root 'top#main'
  get 'top/main'
end
